import React, { Component } from "react";
import "./App.scss";
import "./IconLib";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import AuthComponent from "./app/Auth/Auth";
class App extends Component {
  render() {
    return (
      <div className="App">
        <BrowserRouter>
          <div>
            <Route path="/auth" component={AuthComponent} />
          </div>
        </BrowserRouter>
      </div>
    );
  }
}

export default App;
