import { library } from "@fortawesome/fontawesome-svg-core";
import { fab } from "@fortawesome/free-brands-svg-icons";
import {
  faCheckSquare,
  faCircleNotch,
  faTimesCircle,
  faAngleDown
} from "@fortawesome/free-solid-svg-icons";
import { dom } from "@fortawesome/fontawesome-svg-core";

library.add(fab, faCheckSquare, faCircleNotch, faTimesCircle, faAngleDown);
dom.watch();
