import React from "react";
import "../signup.scss";
import InfoCard from "../../../components/InfoCard";
import Dropdown from "../../../components/Dropdown";
import Field from "../../../components/Field";
import Divider from "../../../components/Divider";

export default class BasicInfoForm extends React.Component {
  countries = [
    {
      label: "India",
      value: "INDIA"
    },
    {
      label: "Srilanka",
      value: "SRI_LANKA"
    }
  ];

  state = {
    loading: false,
    name: "",
    email: "",
    phone: "",
    city: "",
    country: null
  };
  handleChange = name => event => this.setState({ [name]: event.target.value });
  handleNext = () => {
    this.props.onNext();
  };
  handleCountryChange = country => this.setState({ country });
  render() {
    const { country } = this.state;
    const isDisabled =
      Object.keys(this.state).filter(
        key => key !== "loading" && !this.state[key]
      ).length !== 0;
    return (
      <InfoCard
        title="Basic Information"
        isDisabled={isDisabled}
        onSubmit={this.onSubmit}>
        <div className="columns flex-wrap">
          <Field
            classes="column is-one-third"
            label="Name"
            name="name"
            object={this.state}
            func={this.handleChange}
            placeholder="e.g Alex Smith"
          />
          <Field
            classes="column is-one-third"
            label="Email Address"
            name="email"
            object={this.state}
            func={this.handleChange}
            placeholder="e.g alex.smith@gmail.com"
          />
          <Field
            classes="column is-one-third"
            label="Phone"
            name="phone"
            object={this.state}
            func={this.handleChange}
            placeholder="e.g +144-5454345"
          />
          <Field
            classes="column is-one-third"
            label="City"
            name="city"
            object={this.state}
            func={this.handleChange}
            placeholder="eg. Palo Alto"
          />
          <div className="field column is-one-third">
            <Dropdown
              title="Country"
              items={this.countries}
              active={country}
              onchange={this.handleCountryChange}
            />
          </div>
        </div>
        <Divider />
      </InfoCard>
    );
  }
}
