import React from "react";
import "../signup.scss";
import Dropdown from "../../../components/Dropdown";
import Field from "../../../components/Field";

// const EducationTextField = ({
//   object,
//   label,
//   secondaryLabel,
//   name,
//   placeholder,
//   func,
//   helper,
//   classes = ""
// }) => (
//   <div className={"field padding-b-8 " + classes}>
//     {label && <label className="label is-title-2">{label}</label>}
//     {secondaryLabel && (
//       <label className="label is-caption has-text-grey">{secondaryLabel}</label>
//     )}
//     <div className="control">
//       <input
//         className="input is-body-1"
//         name={name}
//         value={object[name]}
//         type="text"
//         placeholder={placeholder}
//         onChange={func(name)}
//       />
//       {helper && <p className="help is-caption has-text-grey">{helper}</p>}
//     </div>
//   </div>
// );

export default class EducationForm extends React.Component {
  qualifications = [
    {
      label: "Post Graduate Degree",
      value: "POST_GRADUATE"
    },
    {
      label: "Graduate Degree",
      value: "GRADUATE"
    },
    {
      label: "Under Graduate Degree",
      value: "UNDER_GRADUATE"
    },
    {
      label: "Pre University / XII",
      value: "PU_XII"
    },
    {
      label: "High School",
      value: "HIGH_SCHOOL"
    }
  ];

  state = {
    loading: false,
    education: {
      college: "",
      from: "",
      to: "",
      qualification: null
    }
  };

  // handle form change
  handleChange = name => event => {
    const { education } = this.state;
    education[name] = event.target.value;
    this.setState({ education });
  };

  // handle on next button click
  handleNext = () => {
    this.props.onNext();
  };

  // resets current education form
  resetEducation = () => this.setState({ education: this.getEmptyEducation() });

  // on addition of new education
  onAdd = () => {
    this.setState({ loading: true });
    setTimeout(() => {
      const { education } = this.state;
      const newEducation = Object.create(education);
      this.resetEducation();
      this.setState({ loading: false });
      this.props.onNewEducation(newEducation);
    }, 500);
  };

  // gets empty education data
  getEmptyEducation = () => {
    return Object.create({
      college: "",
      from: "",
      to: "",
      qualification: null
    });
  };

  // set qualification from dropdown
  setQualification = qual => {
    const { education } = this.state;
    education["qualification"] = qual;
    this.setState({ education });
  };

  render() {
    const { education } = this.state;
    const { qualification, college, from, to } = education;
    // disabled state for adding new education
    const isAddNewEnabled = college && from && to && qualification;
    // state to enable Reset option only when any of the field has a value
    const isResetEnabled = college || from || to || qualification;

    return (
      <div>
        <div className="level">
          <div className="level-left is-title-1 has-text-weight-semibold has-text-black">
            + Add Education
          </div>
          {isResetEnabled && (
            <div className="level-right">
              <a
                className="button is-white is-danger is-small"
                onClick={this.resetEducation}>
                Reset
              </a>
            </div>
          )}
        </div>
        <div className="Educations">
          <Dropdown
            title="Qualification"
            active={qualification}
            items={this.qualifications}
            onchange={this.setQualification}
          />
          <Field
            label="College/University"
            object={education}
            placeholder="Place where you studied/studying"
            name="college"
            classes="w-50"
            func={this.handleChange}
          />

          <label className="label is-title-2">Duration</label>
          <div style={{ display: "flex" }}>
            <div style={{ flexBasis: "40%", paddingRight: 8 }}>
              <Field
                secondaryLabel="From"
                object={education}
                placeholder="ex: March 2018"
                name="from"
                func={this.handleChange}
                helper={`Enter date in "Month Year" format`}
              />
            </div>
            <div style={{ flexBasis: "40%" }}>
              <Field
                secondaryLabel="To"
                object={education}
                placeholder="ex: Decemeber 2018"
                name="to"
                func={this.handleChange}
                helper={`Enter date in "Month Year" format. Leave it blank if currently studying here`}
              />
            </div>
          </div>
        </div>

        <button
          onClick={this.onAdd}
          disabled={!isAddNewEnabled}
          className="button is-primary margin-t-16">
          Add Entry
        </button>
      </div>
    );
  }
}
