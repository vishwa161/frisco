import React from "react";
import "../signup.scss";
import InfoCard from "../../../components/InfoCard";
import Divider from "../../../components/Divider";
import EducationForm from "./EducationForm";

const qualifications = [
  {
    label: "Post Graduate Degree",
    value: "POST_GRADUATE"
  },
  {
    label: "Graduate Degree",
    value: "GRADUATE"
  },
  {
    label: "Under Graduate Degree",
    value: "UNDER_GRADUATE"
  },
  {
    label: "Pre University / XII",
    value: "PU_XII"
  },
  {
    label: "High School",
    value: "HIGH_SCHOOL"
  }
];

const EducationItem = ({ title, value, basis = "20%" }) => (
  <div
    style={{
      flexBasis: basis,
      minWidth: 0,
      padding: "8px 0px"
    }}>
    <div className="is-title-2 has-text-grey">{title}</div>
    {value && <div className="is-body-1 break-word">{value}</div>}
  </div>
);

function EducationView({ college, qualification, from, to }) {
  return (
    <div className="EducationView">
      <EducationItem
        title="Qualification"
        basis="20%"
        value={qualifications.find(q => q.value === qualification).label}
      />
      <EducationItem title="College" basis="35%" value={college} />
      <EducationItem title="From" value={from} />
      <EducationItem title="To" value={to ? to : "Present"} />
    </div>
  );
}

export default class Educations extends React.Component {
  state = {
    loading: false,
    educations: [
      {
        college: "Gogte Institute of Technology",
        from: "March 2018",
        to: "August 2018",
        qualification: "GRADUATE"
      }
    ]
  };

  onNewEducation = education => {
    let { educations = [] } = this.state;
    educations = educations.push(education);
    this.setState({ educations });
  };

  render() {
    const { educations } = this.state;
    // disabled state for next button
    const isNextDisabled = educations.length === 0;
    // render all the provided educations
    const renderEducations =
      educations.length > 0 ? (
        educations.map((e, i) => (
          <React.Fragment key={i}>
            <EducationView {...e} />
            {i !== educations.length - 1 && <Divider yMargin={16} />}
          </React.Fragment>
        ))
      ) : (
        <div className="is-size-6  has-text-grey-light">
          No entries are made yet
        </div>
      );

    return (
      <InfoCard
        title="Qualifications"
        isDisabled={isNextDisabled}
        onSubmit={this.onSubmit}
        secActionText="Back">
        <div className="is-title-1 has-text-black has-text-weight-semibold padding-b-8">
          Summary
        </div>
        {renderEducations}
        <Divider extra={25} />
        <EducationForm
          onNewEducation={this.onNewEducation}
          educations={educations}
        />
      </InfoCard>
    );
  }
}
