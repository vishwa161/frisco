import React from "react";
import "../signup.scss";
import InfoCard from "../../../components/InfoCard";
import Divider from "../../../components/Divider";
import ExperienceForm from "./ExperienceForm";

const ExperienceItem = ({
  title,
  value,
  arr,
  flexValue = 1,
  noflex = false
}) => (
  <div
    style={{
      flex: !noflex ? `${flexValue} ${flexValue} ${flexValue * 110}px` : null,
      padding: "8px 0px",
      minWidth: 0
    }}>
    <div className="is-title-2 has-text-grey">{title}</div>
    {value && <div className="is-body-1 break-word">{value}</div>}
    {arr &&
      arr.map((val, i) => (
        <div key={i} className="is-body-1 break-word padding-t-4">
          <span className="has-text-grey-lighter">&bull;</span> {val.trim()}
        </div>
      ))}
  </div>
);

function ExperienceView({
  company,
  designation,
  location,
  responsibilities,
  from,
  to
}) {
  return (
    <div className="ExperienceView">
      <ExperienceItem title="Company" flexValue={1.5} value={company} />
      <ExperienceItem title="Designation" flexValue={1.5} value={designation} />
      <ExperienceItem title="Location" flexValue={1.5} value={location} />
      <ExperienceItem title="From" value={from} />
      <ExperienceItem title="To" value={to ? to : "Present"} />
      {responsibilities && (
        <ExperienceItem
          noflex
          title="Responsibilities"
          arr={responsibilities.split(",")}
        />
      )}
    </div>
  );
}

export default class Experience extends React.Component {
  state = {
    loading: false,
    experiences: [
      {
        _id: "1",
        company: "VMware",
        designation: "Software Engineer",
        location: "Bengaluru, India",
        responsibilities:
          "Responsible for building and handling high-scale distributed systems, Received `Best Employee Award` in 2016",
        from: "August 2018",
        to: ""
      },
      {
        _id: "2",
        company: "Google",
        designation: "Software Engineer",
        location: "Bengaluru, India",
        responsibilities:
          "Responsible for building and handling high-scale distributed systems, Received `Best Employee Award` in 2016",
        from: "August 2016",
        to: "December 2017"
      }
    ]
  };

  onNewExperience = experience => {
    let { experiences = [] } = this.state;
    experiences = experiences.push(experience);
    this.setState({ experiences });
  };

  render() {
    const { experiences } = this.state;
    // disabled state for next button
    const isNextDisabled = experiences.length === 0;
    // render all the provided experiences
    const renderExperiences =
      experiences.length > 0 ? (
        experiences.map((e, i) => (
          <React.Fragment key={i}>
            <ExperienceView {...e} />
            {i !== experiences.length - 1 && <Divider yMargin={16} />}
          </React.Fragment>
        ))
      ) : (
        <div className="is-size-6  has-text-grey-light">
          No experiences provided yet
        </div>
      );

    return (
      <InfoCard
        title="Corporate Experience"
        isDisabled={isNextDisabled}
        onSubmit={this.onSubmit}>
        <div className="is-title-1 has-text-black has-text-weight-semibold padding-b-8">
          Summary
        </div>
        {renderExperiences}
        <Divider extra={25} />
        <ExperienceForm
          onNewExperience={this.onNewExperience}
          experiences={experiences}
        />
      </InfoCard>
    );
  }
}
