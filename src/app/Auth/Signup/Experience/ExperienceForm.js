import React from "react";
import "../signup.scss";
import Field from "../../../components/Field";

// const ExperienceTextField = ({
//   object,
//   label,
//   name,
//   placeholder,
//   func,
//   helper
// }) => (
//   <div className="field">
//     <label className="label is-title-2">{label}</label>
//     <div className="control">
//       <input
//         className="input is-body-1"
//         name={name}
//         value={object[name]}
//         type="text"
//         placeholder={placeholder}
//         onChange={func(name)}
//       />
//       {helper && <p className="help is-caption has-text-grey">{helper}</p>}
//     </div>
//   </div>
// );

export default class ExperienceForm extends React.Component {
  state = {
    loading: false,
    experience: {
      company: "",
      designation: "",
      location: "",
      responsibilities: "",
      from: "",
      to: ""
    }
  };

  // handle form change
  handleChange = name => event => {
    const { experience } = this.state;
    experience[name] = event.target.value;
    this.setState({ experience });
  };

  // handle on next button click
  handleNext = () => {
    this.props.onNext();
  };

  // resets current experience form
  resetCurrentExp = () =>
    this.setState({ experience: this.getEmptyExperience() });

  // on addition of new experience
  onAdd = () => {
    this.setState({ loading: true });
    setTimeout(() => {
      const { experience } = this.state;
      const newExperience = Object.create(experience);
      this.resetCurrentExp();
      this.setState({ loading: false });
      this.props.onNewExperience(newExperience);
    }, 500);
  };

  // gets empty experience data
  getEmptyExperience = () => {
    return Object.create({
      company: "",
      designation: "",
      location: "",
      responsibilities: "",
      from: "",
      to: ""
    });
  };

  render() {
    const { experience } = this.state;
    const { company, designation, location, from, to } = experience;
    // disabled state for adding new experience
    const isAddNewEnabled = company && designation && location && from && to;
    // state to enable Reset option only when any of the field has a value
    const isResetEnabled = company || designation || location || from || to;

    return (
      <div>
        <div className="level">
          <div className="level-left is-title-1 has-text-black has-text-weight-semibold">
            + Add Experience
          </div>
          {isResetEnabled && (
            <div className="level-right">
              <a
                className="button is-white is-danger is-small"
                onClick={this.resetCurrentExp}>
                Reset
              </a>
            </div>
          )}
        </div>
        <div className="Experience">
          <Field
            label="Company"
            object={experience}
            placeholder="ex: Microsoft"
            name="company"
            func={this.handleChange}
          />

          <Field
            label="Designation"
            object={experience}
            placeholder="ex: Software Engineer"
            name="designation"
            func={this.handleChange}
          />

          <Field
            label="Location"
            object={experience}
            placeholder="ex: Bengaluru, India"
            name="location"
            func={this.handleChange}
          />

          <Field
            label="From"
            object={experience}
            placeholder="ex: March 2018"
            name="from"
            func={this.handleChange}
            helper={`Enter date in "Month Year" format`}
          />
          <Field
            label="To"
            object={experience}
            placeholder="ex: Decemeber 2018"
            name="to"
            func={this.handleChange}
            helper={`Enter date in "Month Year" format. If currently 
            working here then leave it blank`}
          />
        </div>
        <div className="field padding-y-16">
          <label className="label is-title-2">
            Responsibilities & Accomplishments
          </label>
          <div className="control">
            <textarea
              rows="3"
              value={experience.responsibilities}
              resize="none"
              className="textarea"
              name="responsibilities"
              placeholder="Breif out your responsibilities and accomplishments in this company. eg. Responsible for building and handling high-scale distributed systems, Received 'Best Employeee` Award, etc.,"
              onChange={this.handleChange("responsibilities")}
            />
            <p className="help">
              Enter your responsibilities and accomplishments separated by
              comma(,)
            </p>
          </div>
        </div>
        <button
          onClick={this.onAdd}
          disabled={!isAddNewEnabled}
          className="button is-primary margin-t-8">
          Add Experience
        </button>
      </div>
    );
  }
}
