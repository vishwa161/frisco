import React, { Component } from "react";
import Field from "../../../components/Field";
import Divider from "../../../components/Divider";
import AccountHandle from "./AccountHandle";

export default class Account extends Component {
  state = {
    password: "",
    confirmPassword: "",
    handle: ""
  };

  handleChange = name => event => this.setState({ [name]: event.target.value });

  render() {
    const { password, confirmPassword } = this.state;
    return (
      <div>
        <div className="is-size-6 has-text-black has-text-weight-semibold padding-b-8">
          Account Information
        </div>
        <div className="columns padding-t-8">
          <Field
            name="password"
            type="password"
            label="Set Password"
            helper="Minimum 6 characters"
            value={password}
            func={this.handleChange}
            classes="column is-one-third"
          />
          <Field
            name="confirmPassword"
            type="password"
            label="Confirm Password"
            value={confirmPassword}
            func={this.handleChange}
            classes="column is-one-third"
          />
        </div>
        <Divider />
        <AccountHandle onchange={this.handleChange} />
      </div>
    );
  }
}
