import React from "react";
import names from "classnames";

export default class AccountHandle extends React.Component {
  state = {
    handleStatus: 0, // -2: invalid, -1: not-avail, 0: not checked, 1: avail
    loading: false,
    handle: "",
    message: ""
  };

  handleChange = event => {
    this.setState({ handle: event.target.value, handleStatus: 0 });
  };

  getIconRender = () => {
    const { handleStatus } = this.state;
    let status, icon;
    if (handleStatus === 0) {
      return;
    } else if (handleStatus <= -1) {
      icon = "fa-times-circle";
      status = "has-text-danger";
    } else if (handleStatus === 1) {
      icon = "fa-check-square";
      status = "has-text-success";
    }

    return (
      <span className={names("icon is-right", status)}>
        <i className={`fas ${icon}`} />
      </span>
    );
  };

  getMessageRender = () => {
    const { handleStatus, handle } = this.state;
    let message, status;
    switch (handleStatus) {
      case -2: {
        message =
          "Invalid format. Handle can have only alphanumerics, period(.), underscore(_) and hypen(-)";
        status = "has-text-danger";
        break;
      }
      case -1: {
        message = `'${handle}' is already taken. Try any another handle.`;
        status = "has-text-danger";
        break;
      }
      case 1: {
        message = `'${handle}' is available`;
        status = "has-text-success";
        break;
      }
      case 0:
      default:
        message = status = "";
    }
    return <p className={names("help is-caption", status)}>{message}</p>;
  };

  checkAvailibility = () => {
    this.setState({ loading: true });
    const { handle } = this.state;
    const rex = new RegExp(/^[a-z]+[0-9]*[._-]?[a-z0-9]+$/);
    setTimeout(() => {
      this.setState({
        handleStatus: rex.exec(handle) === null ? -2 : 1,
        loading: false
      });
    }, 500);
  };

  reset = () => {
    this.setState({ handle: "", handleStatus: 0 });
  };

  render() {
    const { handleStatus, handle, loading } = this.state;
    const checkValid = handle && handle.length >= 3;
    return (
      <div>
        <label className="label is-body-1 has-text-weight-semibold">
          Account Handle
        </label>
        <div className="is-body-2 padding-b-4">
          Account handle lets you share your CV to public
        </div>
        <div className="field has-addons padding-b-8">
          <div className="control">
            <div
              style={{
                borderColor: "#dfdfdf",
                borderTopLeftRadius: 5,
                borderBottomLeftRadius: 5,
                borderRight: "none",
                fontSize: 16
              }}
              className="button is-lowercase is-dark is-outlined is-static">
              @
            </div>
          </div>
          <div className="control w-50 has-icons-right">
            <input
              className="input is-body-1 "
              name="handle"
              value={handle}
              type="text"
              placeholder="ex: John_Doe"
              onChange={this.handleChange}
            />

            {handleStatus !== 0 ? (
              this.getMessageRender()
            ) : (
              <p className="help is-caption has-text-grey">
                Account handle can consist of alphanumerics, period(.),
                underscore(_) or hypen(-) and it is case insensitive. Once
                handle is set, cannot be changed later.
              </p>
            )}
            {this.getIconRender()}
          </div>
          <p className="control">
            <a
              onClick={handleStatus === 0 ? this.checkAvailibility : this.reset}
              disabled={!checkValid}
              className={names(
                "button",
                { "is-primary": handleStatus === 0 },
                { "is-white": handleStatus > 0 },
                { "is-loading": loading }
              )}>
              {handleStatus === 0 ? "Check Availibility" : "Reset"}
            </a>
          </p>
        </div>
        {handleStatus === 1 && (
          <div>
            <div className="is-body-2 has-text-grey">Shareable URL </div>
            <code className="is-size-6 has-text-black">{`frisco.io/@${handle}`}</code>
          </div>
        )}
      </div>
    );
  }
}
