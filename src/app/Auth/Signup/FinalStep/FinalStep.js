import React, { Component } from "react";
import "../signup.scss";
import InfoCard from "../../../components/InfoCard";
import Divider from "../../../components/Divider";
import SkillSelector from "./SkillSelector";
import RecruiterContact from "./RecruiterContact";
import Field from "../../../components/Field";
import Account from "./Account";

function WorthyPointer({ pointer, onchange }) {
  return (
    <div>
      <div className="is-size-6 has-text-black has-text-weight-semibold">
        Worthy Pointers
      </div>
      <div className="field padding-y-8">
        <div className="control">
          <textarea
            rows="3"
            value={pointer}
            resize="none"
            className="textarea"
            name="worthyPointers"
            placeholder="Showcase some of your worth noting accomplishments. For ex: Built and handled the codebase scaling to 5M+ requests per
            minute, Prototyped WASM application in a span of two days without prior expertise, etc."
            onChange={e => onchange("worthyPointers")(e)}
          />
          <p className="help">Enter mutliple pointers separated by comma(,)</p>
        </div>
      </div>
    </div>
  );
}

function RecruiterOption({ onchange, active }) {
  return (
    <div>
      <label className="checkbox">
        <input type="checkbox" checked={active} onChange={onchange} />
        <span className="v-align">
          &nbsp;Allow companies to contact me for job offers
        </span>
      </label>
      {active && <RecruiterContact />}
    </div>
  );
}

export default class FinalStep extends Component {
  state = {
    allowRecruiter: false,
    password: ""
  };

  handleChange = name => e => this.setState({ [name]: e.target.value });

  onRecruiterChange = e => this.setState({ allowRecruiter: e.target.checked });

  render() {
    const isSubmitDisabled = false;
    const { worthyPointers, allowRecruiter, password } = this.state;
    return (
      <div>
        <InfoCard
          title="Final Step"
          isDisabled={isSubmitDisabled}
          onSubmit={this.onSubmit}
          actionText="Create CV"
          secActionText="Back">
          <WorthyPointer
            worthyPointers={worthyPointers}
            onchange={this.handleChange}
          />
          <Divider />
          <SkillSelector />
          <Divider extra={25} />
          <RecruiterOption
            onchange={this.onRecruiterChange}
            active={allowRecruiter}
          />
          <Divider extra={25} />
          <Account onchange={password => this.setState({ password })} />
        </InfoCard>
      </div>
    );
  }
}
