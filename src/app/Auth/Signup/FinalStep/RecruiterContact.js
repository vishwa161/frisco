import React, { Component } from "react";
import Field from "../../../components/Field";
import "../signup.scss";
import Dropdown from "../../../components/Dropdown";

export default class RecruiterContact extends Component {
  state = {
    ctcFrom: "",
    ctcTo: "",
    preferredCities: "",
    mode: null
  };

  commModes = [
    {
      label: "Phone",
      value: "PHONE"
    },
    {
      label: "Email",
      value: "EMAIL"
    },
    {
      label: "Either",
      value: "EITHER"
    }
  ];

  setCommunicationMode = value => {
    this.setState({ mode: value });
  };

  handleChange = name => event => this.setState({ [name]: event.target.value });

  render() {
    const { mode } = this.state;
    return (
      <div>
        <div className="is-title-2 has-text-grey padding-t-16 padding-b-8">
          Let us know your criteria so that you will not receive irrelevant
          calls from recruiters. You can subscribe / unsubscribe to this option
          later too.
        </div>
        <div className="columns padding-y-4">
          <div className="column is-two-fifths">
            <label className="label is-title-2">
              Expected CTC Range (in $)
            </label>
            <div className="columns">
              <Field
                name="ctcFrom"
                object={this.state}
                placeholder="Start Range"
                classes="column"
                helper="Min"
                func={this.handleChange}
              />
              <Field
                name="ctcTo"
                classes="column"
                object={this.state}
                helper="Max"
                placeholder="End Range"
                func={this.handleChange}
              />
            </div>
          </div>
          <div className="column is-two-fifths">
            <Field
              name="preferredCities"
              label="Preferred City / Cities"
              object={this.state}
              placeholder="ex: Bengaluru, Hyderabad"
              helper="If you have multiple preferences, separate them with comma(,)"
              func={this.handleChange}
            />
          </div>
          <div className="column">
            <Dropdown
              title="Mode of Communication"
              active={mode}
              items={this.commModes}
              onchange={this.setCommunicationMode}
            />
          </div>
        </div>
      </div>
    );
  }
}
