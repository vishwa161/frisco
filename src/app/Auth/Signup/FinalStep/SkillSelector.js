import React from "react";
import "../signup.scss";
import AutoSuggestion from "../../../components/AutoSuggestor";

export default class SkillSelector extends React.Component {
  items = [
    { label: "Java", value: "1" },
    { label: "JavaScript", value: "2" },
    { label: "Golang", value: "3" },
    { label: "Python", value: "4" },
    { label: "React", value: "5" },
    { label: "Vue", value: "6" }
  ];
  state = {
    filtered: [],
    selected: []
  };

  contains = (val1, val2) =>
    val1.toLowerCase().includes(val2.trim().toLowerCase());

  filterAlreadySelected = givenItem =>
    this.state.selected.findIndex(item => item.value === givenItem.value) < 0;

  onChange = val => {
    const filtered = this.items
      .filter(item => this.contains(item.label, val))
      .filter(this.filterAlreadySelected);
    this.setState({ filtered });
  };

  onSelect = (value, newSkill) => {
    const { selected } = this.state;
    const item = value
      ? this.items.find(item => item.value === value)
      : { label: newSkill, value: 999 * selected.length };
    selected.push(item);
    this.setState({ selected });
  };

  onRemove = value => {
    const index = this.items.findIndex(item => item.value === value);
    let { selected } = this.state;
    selected.splice(index, 1);
    this.setState({ selected });
  };

  render() {
    const { selected, filtered } = this.state;

    const renderSelected =
      selected.length > 0 ? (
        selected.map((s, i) => (
          <div key={i} className="control padding-t-16">
            <div className="tags has-addons">
              <a className="tag is-link">{s.label}</a>
              <a
                className="tag is-delete"
                onClick={_ => this.onRemove(s.value)}>
                {}
              </a>
            </div>
          </div>
        ))
      ) : (
        <div className="control is-body-2 padding-t-8 has-text-grey">
          No skills entered. Select atleast one and maximum five skills
        </div>
      );

    const acceptNewSkills = selected.length < 5;

    return (
      <div>
        <div className="is-size-6 has-text-black has-text-weight-semibold">
          Primary Skills
        </div>
        <div className="field is-grouped is-grouped-multiline">
          {renderSelected}
        </div>
        {acceptNewSkills ? (
          <div className="field padding-t-8">
            <div className="control">
              <AutoSuggestion
                items={filtered}
                onchange={this.onChange}
                onselect={this.onSelect}
              />
            </div>
          </div>
        ) : (
          <p className="help has-text-grey">Maximum five skills allowed.</p>
        )}
      </div>
    );
  }
}
