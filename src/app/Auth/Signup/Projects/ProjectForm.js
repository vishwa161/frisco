import React from "react";
import "../signup.scss";
import Field from "../../../components/Field";

// const ProjectTextField = ({
//   object,
//   label,
//   name,
//   placeholder,
//   func,
//   helper,
//   classes = ""
// }) => (
//   <div className={"field padding-b-8 " + classes}>
//     <label className="label is-title-2">{label}</label>
//     <div className="control">
//       <input
//         className="input is-body-1"
//         name={name}
//         value={object[name]}
//         type="text"
//         placeholder={placeholder}
//         onChange={func(name)}
//       />
//       {helper && <p className="help is-caption has-text-grey">{helper}</p>}
//     </div>
//   </div>
// );

export default class ProjectForm extends React.Component {
  state = {
    loading: false,
    project: {
      title: "",
      description: "",
      link: "",
      from: "",
      to: ""
    }
  };

  // handle form change
  handleChange = name => event => {
    const { project } = this.state;
    project[name] = event.target.value;
    this.setState({ project });
  };

  // handle on next button click
  handleNext = () => {
    this.props.onNext();
  };

  // resets current project form
  resetCurrentExp = () => this.setState({ project: this.getEmptyProject() });

  // on addition of new project
  onAdd = () => {
    this.setState({ loading: true });
    setTimeout(() => {
      const { project } = this.state;
      const newProject = Object.create(project);
      this.resetCurrentExp();
      this.setState({ loading: false });
      this.props.onNewProject(newProject);
    }, 500);
  };

  // gets empty project data
  getEmptyProject = () => {
    return Object.create({
      title: "",
      description: "",
      link: "",
      from: "",
      to: ""
    });
  };

  render() {
    const { project } = this.state;
    const { title, description, link, from, to } = project;
    // disabled state for adding new project
    const isAddNewEnabled = title && description && link && from && to;
    // state to enable Reset option only when any of the field has a value
    const isResetEnabled = title || description || link || from || to;

    return (
      <div>
        <div className="level">
          <div className="level-left is-title-1 has-text-black has-text-weight-semibold">
            + Add Project / Course / Test
          </div>
          {isResetEnabled && (
            <div className="level-right">
              <a
                className="button is-white is-danger is-small"
                onClick={this.resetCurrentExp}>
                Reset
              </a>
            </div>
          )}
        </div>
        <div className="Projects">
          <Field
            label="Title"
            object={project}
            placeholder="Title of the project or course or test"
            name="title"
            classes="w-50"
            func={this.handleChange}
          />

          <div className="field w-75">
            <label className="label is-title-2">Brief Description</label>
            <div className="control">
              <textarea
                rows="3"
                value={project.responsibilities}
                resize="none"
                className="textarea"
                name="responsibilities"
                placeholder="Breif description about this entry on What and How"
                onChange={this.handleChange("responsibilities")}
              />
              <p className="help has-text-grey">
                Enter your responsibilities and accomplishments separated by
                comma(,)
              </p>
            </div>
          </div>

          <Field
            label="Supporting Link"
            object={project}
            placeholder="Enter website link, github or other link supporting to this entry"
            name="link"
            func={this.handleChange}
            classes="w-75"
          />

          <Field
            label="From (if applicable)"
            object={project}
            placeholder="ex: March 2018"
            name="from"
            func={this.handleChange}
            classes="w-50"
            helper={`Enter date in "Month Year" format. Leave it blank if not applicable.`}
          />
          <Field
            label="To (if applicable)"
            object={project}
            placeholder="ex: Decemeber 2018"
            name="to"
            func={this.handleChange}
            classes="w-50"
            helper={`Enter date in "Month Year" format. Leave it blank if not applicable.`}
          />
        </div>

        <button
          onClick={this.onAdd}
          disabled={!isAddNewEnabled}
          className="button is-primary margin-t-16">
          Add Entry
        </button>
      </div>
    );
  }
}
