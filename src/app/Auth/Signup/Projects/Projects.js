import React from "react";
import "../signup.scss";
import InfoCard from "../../../components/InfoCard";
import Divider from "../../../components/Divider";
import ProjectForm from "./ProjectForm";

const ProjectItem = ({ title, value, arr, basis = "20%" }) => (
  <div
    style={{
      flexBasis: basis,
      minWidth: 0,
      padding: "8px 0px"
    }}>
    <div className="is-title-2 has-text-grey">{title}</div>
    {value && <div className="is-body-1 break-word">{value}</div>}
    {arr &&
      arr.map((val, i) => (
        <div key={i} className="is-body-1 break-word padding-t-4">
          <span className="has-text-grey-lighter">&bull;</span> {val.trim()}
        </div>
      ))}
  </div>
);

function ProjectView({ title, description, link, from, to }) {
  return (
    <div className="ProjectView">
      <ProjectItem title="Title" basis="30%" value={title} />
      {from && (
        <React.Fragment>
          <ProjectItem title="From" value={from} />
          <ProjectItem title="To" value={to ? to : "Present"} />
        </React.Fragment>
      )}
      {link && <ProjectItem title="Supporting Link" basis="60%" value={link} />}
      <ProjectItem
        title="Description"
        basis="60%"
        arr={description.split(",")}
      />
    </div>
  );
}

export default class Projects extends React.Component {
  state = {
    loading: false,
    projects: [
      {
        title: "Healthcare Application",
        description:
          "NDA Signed. Healthcare application built using ReactJS and NodeJS ",
        link: "https://google.com",
        from: "March 2018",
        to: "August 2018"
      }
    ]
  };

  onNewProject = project => {
    let { projects = [] } = this.state;
    projects = projects.push(project);
    this.setState({ projects });
  };

  render() {
    const { projects } = this.state;
    // disabled state for next button
    const isNextDisabled = projects.length === 0;
    // render all the provided projects
    const renderProjects =
      projects.length > 0 ? (
        projects.map((e, i) => (
          <React.Fragment key={i}>
            <ProjectView {...e} />
            {i !== projects.length - 1 && <Divider yMargin={16} />}
          </React.Fragment>
        ))
      ) : (
        <div className="is-size-6  has-text-grey-light">
          No entries are made yet
        </div>
      );

    return (
      <InfoCard
        title="Projects / Courses / Tests"
        isDisabled={isNextDisabled}
        onSubmit={this.onSubmit}
        secActionText="Back">
        <div className="is-title-1 has-text-black has-text-weight-semibold padding-b-8">
          Summary
        </div>
        {renderProjects}
        <Divider extra={25} />
        <ProjectForm onNewProject={this.onNewProject} projects={projects} />
      </InfoCard>
    );
  }
}
