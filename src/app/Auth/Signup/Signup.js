import React from "react";
import "./signup.scss";
import BasicInfoForm from "./BasicInfo/BasicInfo";
import Experience from "./Experience/Experience";
import Projects from "./Projects/Projects";
import Educations from "./Educations/Educations";
import FinalStep from "./FinalStep/FinalStep";

const Banner = props => (
  <section className="hero">
    <div className="hero-body">
      <div className="container">
        <h1 className="title">Welcome</h1>
        <h2 className="subtitle">Let's create an amazing CV for you</h2>
      </div>
    </div>
  </section>
);

export default class Signup extends React.Component {
  state = {
    currentState: 1,
    progress: 90
  };

  onNext = () => this.setState(prev => ({ progress: prev.progress + 10 }));

  renderFormByState = state => {
    switch (state) {
      case 1:
        return <BasicInfoForm onNext={this.onNext} />;
      case 2:
        return <Experience onNext={this.onNext} />;
      case 3:
        return <Projects onNext={this.onNext} />;
      case 4:
        return <Educations onNext={this.onNext} />;
      default:
        return <FinalStep onNext={this.onNext} />;
    }
  };

  render() {
    const { currentState, progress } = this.state;
    return (
      <div>
        <Banner />
        <progress
          className="progress is-link is-small"
          value={progress}
          max="100"
        />
        {this.renderFormByState(currentState)}
      </div>
    );
  }
}
