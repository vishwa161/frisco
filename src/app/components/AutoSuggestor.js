import React, { Component } from "react";
import PropTypes from "prop-types";
import Divider from "./Divider";

export default class AutoSuggestion extends Component {
  state = {
    open: false,
    input: ""
  };

  handleInput = e => {
    const value = e.target.value;
    if (value && value.length > 2) {
      this.props.onchange(value);
    }
    this.setState({ input: value });
    this.changeDropdownState(Boolean(value));
  };

  changeDropdownState = val => this.setState({ open: val });

  getLabelByVal = val =>
    this.props.items.find(item => item.value === val).label;

  onSelect = (val, newSkill) => {
    this.props.onselect(val, newSkill);
    this.setState({ input: "", open: false });
  };

  render() {
    const { open, input } = this.state;
    const { title, items } = this.props;
    const renderItems =
      items.length > 0 ? (
        items.map((item, index) => (
          <div key={index} onClick={_ => this.onSelect(item.value)}>
            <a
              className="dropdown-item"
              style={{
                borderBottom:
                  index !== items.length - 1 ? "1px solid #efefef" : null,
                padding: "8px"
              }}>
              {item.label}
            </a>
          </div>
        ))
      ) : (
        <div className="dropdown-item">
          <div className="has-text-grey is-size-7">No Skills Found</div>
          <div
            onClick={_ => this.onSelect(null, input)}
            className="padding-t-4 button is-white has-text-primary is-paddingless">
            Add This Skill
          </div>
        </div>
      );
    const showDropdown = input && input.length > 2;
    return (
      <div className="padding-b-16">
        {title && <label className="label is-title-2">{title}</label>}
        <div className="control">
          <div className={`dropdown ${open ? "is-active" : null}`}>
            <div className="dropdown-trigger">
              <input
                className="input is-body-1"
                name="suggest"
                value={input}
                type="text"
                placeholder="Start typing Skills"
                onChange={this.handleInput}
              />
            </div>
            {showDropdown && (
              <div className="dropdown-menu" id="dropdown-menu" role="menu">
                <div className="dropdown-content">{renderItems}</div>
              </div>
            )}
          </div>
        </div>
      </div>
    );
  }
}

// AutoSuggestion.propTypes = {
//   active: PropTypes.string,
//   title: PropTypes.string.isRequired,
//   items: PropTypes.arrayOf(
//     PropTypes.shape({
//       label: PropTypes.string.isRequired,
//       value: PropTypes.string.isRequired
//     })
//   ).isRequired,
//   onchange: PropTypes.func.isRequired
// };
