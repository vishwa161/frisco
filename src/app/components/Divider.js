import React from "react";

const Divider = ({
  extra = 0,
  extraLeft = 0,
  extraRight = 0,
  yMargin = 16
}) => {
  let marginLeft = 0;
  let marginRight = 0;
  if (extra > 0) {
    marginLeft = marginRight = -extra;
  } else if (extraLeft > 0) {
    marginLeft = -extra;
  } else if (extraRight > 0) {
    marginRight = -extra;
  }
  return (
    <div
      style={{
        borderBottom: "1px solid rgba(0,0,0,0.12)",
        marginTop: yMargin,
        marginBottom: yMargin,
        marginLeft,
        marginRight
      }}
    />
  );
};

export default Divider;
