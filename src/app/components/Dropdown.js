import React, { Component } from "react";
import PropTypes from "prop-types";

export default class Dropdown extends Component {
  state = {
    open: false
  };

  toggleState = () => this.setState(prev => ({ open: !prev.open }));

  getLabelByVal = val =>
    this.props.items.find(item => item.value === val).label;

  onSelect = val => {
    this.props.onchange(val);
    this.toggleState();
  };

  render() {
    const { open } = this.state;
    const { title, active, items } = this.props;

    const renderItems = items.map((item, index) => (
      <a
        key={index}
        className={`dropdown-item ${
          active === item.value ? "is-active" : null
        }`}
        onClick={_ => this.onSelect(item.value)}>
        {item.label}
      </a>
    ));

    return (
      <div className="padding-b-16">
        <label className="label is-title-2">{title}</label>
        <div className="control">
          <div className={`dropdown ${open ? "is-active" : null}`}>
            <div className="dropdown-trigger">
              <button
                className="button"
                aria-haspopup="true"
                onClick={this.toggleState}
                aria-controls="dropdown-menu">
                <span className="has-text-grey">
                  {active ? this.getLabelByVal(active) : "-- Choose --"}
                </span>
                <span className="icon is-small">
                  <i className="fas fa-angle-down" aria-hidden="true" />
                </span>
              </button>
            </div>
            <div className="dropdown-menu" id="dropdown-menu" role="menu">
              <div className="dropdown-content">{renderItems}</div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

Dropdown.propTypes = {
  active: PropTypes.string,
  title: PropTypes.string.isRequired,
  items: PropTypes.arrayOf(
    PropTypes.shape({
      label: PropTypes.string.isRequired,
      value: PropTypes.string.isRequired
    })
  ).isRequired,
  onchange: PropTypes.func.isRequired
};
