import React from "react";

const Field = ({
  object,
  value,
  label,
  type = "text",
  secondaryLabel,
  name,
  placeholder,
  func,
  helper,
  classes = ""
}) => (
  <div className={"field padding-b-8 " + classes}>
    {label && <label className="label is-title-2">{label}</label>}
    {secondaryLabel && (
      <label className="label is-caption has-text-grey">{secondaryLabel}</label>
    )}
    <div className="control">
      <input
        className="input is-body-1"
        name={name}
        value={value !== undefined ? value : object[name]}
        type={type}
        placeholder={placeholder}
        onChange={func(name)}
      />
      {helper && <p className="help is-caption has-text-grey">{helper}</p>}
    </div>
  </div>
);

export default Field;
