import React from "react";

const InfoCard = ({
  children,
  title,
  isDisabled = false,
  onSubmit,
  actionText = "Next",
  secActionText,
  secActionDisabled,
  onSecAction
}) => {
  return (
    <div className="card">
      <div className="card-header">
        <div className="card-header-title has-text-weight-semibold has-text-primary is-size-5 padding-x-24 padding-y-16">
          {title}
        </div>
      </div>
      <div className="card-content">{children}</div>
      <footer
        className="card-footer"
        onSubmit={this.onSubmit}
        style={{ margin: "10px 0px" }}>
        {secActionText && (
          <a
            disabled={secActionDisabled}
            className="card-footer-item button is-white has-text-grey"
            onClick={onSecAction}>
            {secActionText}
          </a>
        )}
        <a
          disabled={isDisabled}
          className="card-footer-item button is-primary is-inverted"
          onClick={onSubmit}>
          {actionText}
        </a>
      </footer>
    </div>
  );
};

export default InfoCard;
